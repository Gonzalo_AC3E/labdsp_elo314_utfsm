# LabDSP_ELO314_UTFSM

Este repositorio está entendido para los alumnos del Laboratorio de Procesamiento Digital de Señales, ELO-314, del Departamento de Electrónica de la UTFSM.

En este repositorio se dispone todo archivo necesario para comenzar el desarrollo de cada una de las experiencias de laboratorio.

---

## Estructura general del repositorio

```
.
|-- audio_files				: Carpetas con archivos y arreglos de datos de audio, 
|-- DSP_AIC3106				: Librería de soporte al codec de audio de la LCDK
|-- DSP_BSL		 			: Librería de soporte a la LCDK (Board Support Library)
|-- dsp_lab_utils_lib		: Librería de utilidades del laboratoio, DLU (Dsp Lab Utilities)
|-- lab1p2					: Carpetas de archivos fuente y archivos de proyecto CCS (build) para el laboratorio
|-- lab2p2					: Carpetas de archivos fuente y archivos de proyecto CCS (build) para el laboratorio
|-- lab3p2					: Carpetas de archivos fuente y archivos de proyecto CCS (build) para el laboratorio
|-- lab4p2					: Carpetas de archivos fuente y archivos de proyecto CCS (build) para el laboratorio
`-- lab5p2					: Carpetas de archivos fuente y archivos de proyecto CCS (build) para el laboratorio
```

