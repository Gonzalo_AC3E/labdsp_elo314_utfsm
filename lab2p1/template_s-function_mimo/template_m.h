#ifndef TEMPLATE_M_H
#define TEMPLATE_M_H

/******************************************************************************
**      INCLUDES
******************************************************************************/
#include <stdint.h>
#include <stdbool.h>

/******************************************************************************
**      PREPROCESSOR CONSTANTS
******************************************************************************/


/******************************************************************************
**      CONFIGURATION CONSTANTS
******************************************************************************/


/******************************************************************************
**      MACRO DEFINITIONS
******************************************************************************/


/******************************************************************************
**      PUBLIC DATATYPES
******************************************************************************/


/******************************************************************************
**      PUBLIC (GLOBAL) VARIABLES DECLARATIONS
******************************************************************************/


/******************************************************************************
**      PUBLIC FUNCTION PROTOTYPES
******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/*************************************************************************************************/
// Declaración de Funciones 
void multi(double input1, double input2, double *output1, double *output2);

/*************************************************************************************************/
#ifdef __cplusplus
}
#endif
#endif
/******************************************************************************
**      END OF HEADER FILE
******************************************************************************/

