/******************************************************************************
* \file     Lab1p2.c
*
* \brief    Experiencia 2 de laboratorio DSP ELO314
*
* \authors  Gonzalo Carrasco
******************************************************************************/

/******************************************************************************
**      HEADER FILES
******************************************************************************/
#include "dlu_global_defs.h"
#include "L138_LCDK_aic3106_init.h"
#include "dsp_lab_utils.h"
#include <math.h>
#include "dlu_codec_config.h"

/******************************************************************************
**      MODULE PREPROCESSOR CONSTANTS
******************************************************************************/
// Selecci�n de buffer lineal. No definir este flag implica modo buffer circular
#define LINEAR_BUFFER_MODE

/******************************************************************************
**      MODULE MACROS
******************************************************************************/
// MultiTap
#define DELAY_MT_M              (100)               // �para lograr x ms ?
#define DELAY_MT_N              (4)                 // Etapas
#define DELAY_MT_ATTEN_FACTOR   (0.35)

// Buffer
#define AUDIOBUFFERSIZE          (200)              // �para lograr x ms ?

// RMS
#define RMS_WINDOW_SAMPLES       (100)              // �para lograr x ms de ventana?
#define NOISE_FLOOR_RMS          (0.0)              // Valor RMS sin hablar a micr�fono

// Biquads
#define BQ2VARIANTS              (8)                // Variantes en oscilador
#define BQ2TIMERMAXCOUNT         (10)               // �Cuentas para 0.5s?
#define BQAMPLITUDE              (0.8)              // Amplitud de impulso



/******************************************************************************
**      MODULE DATATYPES
******************************************************************************/


/******************************************************************************
**      MODULE VARIABLE DEFINITIONS
******************************************************************************/

/*---------------------------------------------------------------------------*/
/* ENTRADAS Y SALIDAS DEL AIC CODEC */
/*---------------------------------------------------------------------------*/
/*
 * Tipo de dato para el CODEC (Union)
 */
AIC31_data_type codec_data;

/*
 * Varibles de entrada y salida en formato flotante
 */
float floatCodecInputR,floatCodecInputL;
float floatCodecOutputR,floatCodecOutputL;

/*
 * Variables de estado que indica salida saturada
 */
int outSaturationStat = 0;

/*---------------------------------------------------------------------------*/
/* BUFFER DE AUDIO E �NDICES */
/*---------------------------------------------------------------------------*/
//#pragma DATA_SECTION(gAudioBufferL,".EXT_RAM")
//#pragma DATA_SECTION(gAudioBufferR,".EXT_RAM")
float           gAudioBufferL[AUDIOBUFFERSIZE];
float           gAudioBufferR[AUDIOBUFFERSIZE];

float gAudioBufferOutputL = 0.0;
float gAudioBufferOutputR = 0.0;


/*---------------------------------------------------------------------------*/
/* C�MPUTO RMS Y BUFFERS AUXILIARES */
/*---------------------------------------------------------------------------*/
uint32_t        gSampleCounterRmsWindow = 0;
float           gAudioRmsPartial = 0.0;
float           gAudioRms = 0.0;
float           gModRms = 0.0;

/*---------------------------------------------------------------------------*/
/* GENERACI�N DE SE�ALES */
/*---------------------------------------------------------------------------*/
/* Coeficientes para biquiad con polinomio de denominador m�nico en z^-1 */
float gBq1B0 = 0.051342456225139;
float gBq1A1 = -1.997362212708321;
float gBq1A2 = 1.0;

float gBq2B0 = 0.064671642929027;
float gBq2A1 = -1.995813196269491;
float gBq2A2 = 1.0;

/* Para generar un impulso de est�mulo a biquads
 */
float gImpulseGen1 = BQAMPLITUDE;
float gImpulseGen2 = BQAMPLITUDE;

// Estados de Biquad 1
float gBq1Input = 0.0;
float gBq1Output[3] = {0.0,0.0,0.0};
// Estados de Biquad 2
float gBq2Input = 0.0;
float gBq2Output[3] = {0.0,0.0,0.0};

/* Variables de cuenta de tiempo para cambio en biquad 2 */
uint32_t gBq2TimeCounter = 0;
/* Conteo de notas */
uint32_t gBq2Note = 0;

/*---------------------------------------------------------------------------*/
/* VARABLES OVERDRIVE */
/*---------------------------------------------------------------------------*/
float gOverdriveInput = 0;
float gOverdriveOutput = 0;

float gOverdriveInput_gain   =  1.0;
float gOverdriveOutput_gain  =  1.0;
float gOverdriveBeta         =  0.1;
float gOverdriveAlpha        =  0.2;

/*---------------------------------------------------------------------------*/
/* DELAY MULTITAP */
/*---------------------------------------------------------------------------*/
float       gDelayMTOutput = 0.0;       // Salida del Delay multitap
uint32_t    gIdxDelayMTStage = 0;       // Etapa
float       delayMTB[DELAY_MT_N];       // Ganancia por etapa: b_k

/******************************************************************************
**      PRIVATE FUNCTION DECLARATIONS (PROTOTYPES)
******************************************************************************/

/* Notar que se usa el mismo buffer (gAudioBufferL) en memoria tanto para
 * la implementaci�n Lineal como Circular. Por lo tanto se invocar�
 * o a la funci�n de gesti�n lineal o a la funci�n de gesti�n ciruclar
 * excluyentemente. Para eso es el flag LINEAR_BUFFER_MODE
 */
void initAudioBuffers(void);

#ifdef LINEAR_BUFFER_MODE
//#pragma FUNC_ALWAYS_INLINE(delayThroughLinearBufferLeft)

float delayThroughLinearBufferLeft(float bufferInputData);
float delayThroughLinearBufferRight(float bufferInputData);

#else

float delayThroughCircularBufferLeft(float bufferInputData);
float delayThroughCircularBufferRight(float bufferInputData);

#endif


/******************************************************************************
**      FUNCTION DEFINITIONS
******************************************************************************/

interrupt void interrupt4(void) // Interrupt Service Routine
{
//.............................................................................
        /*-------------------------------------------------------------------*/
        /* LECTURA DE ENTRADAS DEL CODEC */
        /*-------------------------------------------------------------------*/
        DLU_readCodecInputs(&floatCodecInputL, &floatCodecInputR);


        /*-------------------------------------------------------------------*/
        /* Inicia medici�n de tiempo de ejecuci�n */
        DLU_tic();
        /*-------------------------------------------------------------------*/
        /* BUFFERS LINEALES Y CIRCULARES */
        /*-------------------------------------------------------------------*/
#ifdef LINEAR_BUFFER_MODE
        /* Buffer lineal */
        gAudioBufferOutputL = delayThroughLinearBufferLeft(floatCodecInputL);

#else

        /* Buffer circular */
        gAudioBufferOutputL = delayThroughCircularBufferLeft(floatCodecInputL);
#endif
        /* Medici�n de tiempo de ejecuci�n */
        DLU_toc();


        /*-------------------------------------------------------------------*/
        /* OSCILADORES BIQUAD */
        /*-------------------------------------------------------------------*/
        /* Actualizaci�n de biquad 1 */

        gBq1Input = gImpulseGen1;

        gBq1Output[2] = gBq1Output[1];
        gBq1Output[1] = gBq1Output[0];
        gBq1Output[0] = (gBq1B0 * gBq1Input) - (gBq1A1 * gBq1Output[1]) - (gBq1A2 * gBq1Output[2]);

        /* Actualiza generador de impulso para que sea cero de aqu� en adelante */
        gImpulseGen1 = 0.0;

        /*-------------------------------------------------------------------*/
        /* Cambio de nota para biquad 2 */


        /* Actualizaci�n de biquad 2 */
        gBq2Input = gImpulseGen2;

        gBq2Output[2] = gBq2Output[1];
        gBq2Output[1] = gBq2Output[0];
        gBq2Output[0] = (gBq2B0 * gBq2Input) - (gBq2A1 * gBq2Output[1]) - (gBq2A2 * gBq2Output[2]);

        /* Actualiza generador de impulso para que sea cero de aqu� en adelante */
        gImpulseGen2 = 0.0;

        /*-------------------------------------------------------------------*/
        /* CALCULO VALOR RMS DE VENTANA: gAudioRms */
        /*-------------------------------------------------------------------*/
        gSampleCounterRmsWindow++;
        if ( gSampleCounterRmsWindow <= RMS_WINDOW_SAMPLES)
        {
            // COMPLETAR
        }
        else
        {
            // COMPLETAR
        }

        /* MODULACI�N CON RMS */
        gModRms = gBq2Output[0] * (gAudioRms - NOISE_FLOOR_RMS);

        /*-------------------------------------------------------------------*/
        /* DISTORSI�N OVERDRIVE */
        /*-------------------------------------------------------------------*/


        /*-------------------------------------------------------------------*/
        /* DELAY MULTITAP */
        /*-------------------------------------------------------------------*/


        /*-------------------------------------------------------------------*/
        /* PARA VISUALIZAR EN GR�FICO */
        /*-------------------------------------------------------------------*/
        DLU_enableSynchronicSingleCaptureOnAllGraphBuff();
        DLU_appendGraphBuff1(floatCodecInputL);
        DLU_appendGraphBuff2(gDelayMTOutput);

        /*-------------------------------------------------------------------*/
        // USO DE ESTADO DE PULSADOR USER1 PARA ACTIVAR LED
        /*-------------------------------------------------------------------*/
        // Obtenci�n de valores de estados para cambiar estado de leds
        // LED 5
        if ( DLU_readToggleStatePB1() )
            DLU_writeLedD5(LED_ON);
        else
            DLU_writeLedD5(LED_OFF);
        // LED 6
        if ( DLU_readToggleStatePB2() )
            DLU_writeLedD6(LED_ON);
        else
            DLU_writeLedD6(LED_OFF);
        // LED 7
        if ( DLU_readToggleStatePB12() )
            DLU_writeLedD7(LED_ON);
        else
            DLU_writeLedD7(LED_OFF);

        /*-------------------------------------------------------------------*/
        /* SELECCI�N DE ESCRITURA EN SALIDA DEL CODEC */
        /*-------------------------------------------------------------------*/
        // BYPASS
        //floatCodecOutputL = floatCodecInputL;
        //floatCodecOutputR = floatCodecInputR;

        // DELAY
        //floatCodecOutputL = gAudioBufferOutputL;
        //floatCodecOutputR = gAudioBufferOutputL;

        // BIQUAD
        //floatCodecOutputL = gBq1Output[0];
        //floatCodecOutputR = gBq2Output[0];

        // Modulaci�n con envolvente RMS
        floatCodecOutputL = gModRms;
        floatCodecOutputR = gModRms;

        //floatCodecOutputL = gOverdriveOutput;
        //floatCodecOutputR = gOverdriveOutput;

        //floatCodecOutputL = gDelayMTOutput;
        //floatCodecOutputR = gDelayMTOutput;

        //---------------------------------------------------------------------
        // Escritura en CODEC
        outSaturationStat = DLU_writeCodecOutputs(floatCodecOutputL,floatCodecOutputR);
        // Enciende LED 4 en caso de saturaci�n de alguna de las salidas
        DLU_writeLedD4((PB_INT_TYPE)outSaturationStat);

//.............................................................................
    return; // Fin de interrupci�n del CODEC
}

//*****************************************************************************
void main()
{
    /* Inicializaci�n de Pulsadores User 1 y User 2 */
    DLU_initPushButtons();
    /* Inicializa funci�n de medici�n de tiempos de ejecuci�n */
    DLU_initTicToc();
    /* Inicializacion BSL y AIC31 Codec */
    L138_initialise_intr(CODEC_FS, CODEC_ADC_GAIN, CODEC_DAC_ATTEN, CODEC_INPUT_CFG);
    /* Inicializaci�n de LEDs */
    DLU_initLeds();

    /*-----------------------------------------------------------------------*/
    /* Inicializaci�n de buffers a cero */
    initAudioBuffers();


   /* Loop infinito a espera de interrupci�n del Codec */
    while(1);
}

/******************************************************************************
*   \brief  Funci�n que inicializa en cero el contenido de los buffers de audio
*
*   \param void
*
*   \return void
******************************************************************************/
void initAudioBuffers(void)
{
    uint32_t idxLinearAudioBuffer = 0;

    for(idxLinearAudioBuffer = 0 ; idxLinearAudioBuffer < AUDIOBUFFERSIZE; idxLinearAudioBuffer++)
    {
        gAudioBufferL[idxLinearAudioBuffer] = 0.0;
        gAudioBufferR[idxLinearAudioBuffer] = 0.0;
    }
    return;
}

/******************************************************************************
*   \brief  Funci�n que recibe una muestra actual de entrada y retorna una
*           muestra AUDIOBUFFERSIZE -esima anterior
*
*   \param bufferInputData : muestra que recibe el buffer como entrada
*
*   \return float muestra (sample) de salida del buffer lineal
******************************************************************************/
float delayThroughLinearBufferLeft(float bufferInputData)
{
    uint32_t idxLinearAudioBuffer = 0;

    // �QU� EST� MALO?

    float outputData = gAudioBufferL[AUDIOBUFFERSIZE-1];
    gAudioBufferL[0] = bufferInputData;
    for(idxLinearAudioBuffer = 0; idxLinearAudioBuffer < AUDIOBUFFERSIZE-1; idxLinearAudioBuffer++)
    {
        gAudioBufferL[idxLinearAudioBuffer+1] = gAudioBufferL[idxLinearAudioBuffer];
    }

    return outputData;
}

/******************************************************************************
*   \brief  Funci�n que recibe una muestra actual de entrada y retorna una
*           muestra AUDIOBUFFERSIZE -esima anterior
*
*   \param bufferInputData : muestra que recibe el buffer como entrada
*
*   \return float muestra (sample) de salida del buffer lineal
******************************************************************************/
float delayThroughCircularBufferLeft(float bufferInputData)
{

    // COMPLETAR

    return bufferInputData;
}


/******************************************************************************
**      END OF SOURCE FILE
******************************************************************************/
