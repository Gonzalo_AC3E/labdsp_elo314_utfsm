/***************************************************************************//**
* \file     L5p2.c
*
* \brief    Base para el laboratorio L4p2
*
* \authors  Gonzalo Carrasco
*******************************************************************************/

/******************************************************************************
**      HEADER FILES
******************************************************************************/
#include "dlu_global_defs.h"
#include "L138_LCDK_aic3106_init.h"
#include "dsp_lab_utils.h"
#include <math.h>
#include "dlu_codec_config.h"
#include "levinson.h"
#include "functions.h"
#include "const_arrays.h"
#include "vowel_a.h"
#include "stdio.h"
#include "string.h"


/******************************************************************************
**      MODULE PREPROCESSOR CONSTANTS
******************************************************************************/

/******************************************************************************
**      MODULE MACROS
******************************************************************************/

/* Macro para no sobrecargar con tanto indexado de punteros y estructuras */
#define codecInputArrayAsInt16L(idx)    ( ((dataPairLR_t *)pInputPairBuffer)[idx].leftChannel)
#define codecInputArrayAsInt16R(idx)    ( ((dataPairLR_t *)pInputPairBuffer)[idx].rightChannel)
#define codecOutputArrayAsInt16L(idx)   ( ((dataPairLR_t *)pOutputPairBuffer)[idx].leftChannel)
#define codecOutputArrayAsInt16R(idx)   ( ((dataPairLR_t *)pOutputPairBuffer)[idx].rightChannel)

/******************************************************************************
**      MODULE DATATYPES
******************************************************************************/
/*
 * Estructura para descomponer en canales la trama del codec en DMA
 */
typedef struct
{
    int16_t rightChannel;
    int16_t leftChannel;
} dataPairLR_t;

typedef enum
{
    VU_UNVOICED = 0,
    VU_VOICED
}vuSound_e;

typedef enum
{
    NOISE_STATIC = 0,
    NOISE_RAND
}noiseType_e;

typedef enum
{
    SYNTHMODE_ALWAYS_VOICED = 0,
    SYNTHMODE_ALWAYS_UNVOICED,
    SYNTHMODE_AUTOMATIC,
    SYNTHMODE_EXTERNALSOURCE
}synthMode_e;

/******************************************************************************
**      MODULE VARIABLE DEFINITIONS
******************************************************************************/

/*---------------------------------------------------------------------------*/
/* VARIABLES AUXILIARES PARA BUFFERS DE TRANSFERENCIA DMA */
/*---------------------------------------------------------------------------*/
/*
 * Punteros a buffers ping y pong (definidos en 'L138_LCDK_aic3106_init.c')
 */
extern int16_t *pingIN, *pingOUT, *pongIN, *pongOUT;

/*
 * Puntero a buffer con pares de entradas Izquierdo/Derecho desde DMA
 */
int16_t *pInputPairBuffer;

/*
 * Puntero a buffer con pares de salidas Izquierdo/Derecho para DMA
 */
int16_t *pOutputPairBuffer;

/*
 * Flag de indicaci�n que un nuevo frame est� listo para procesarse
 */
volatile int gProcessingBufferIsFull = 0;

/*
 * Contador de frames procesados
 */
int gFramesCounter = 0;

/*---------------------------------------------------------------------------*/
/* DEFINICI�N DE VARIABLES GLOBALES PARA LPC */
/*---------------------------------------------------------------------------*/

/*------------------- ARREGLOS SE�ALES DE AUDIO --------------------*/
/*
 * Buffer de entrada izquierda
 */
float gFloatInputBufferL[FRAME_SIZE];

/*
 * Buffer de entrada derecha
 */
float gFloatInputBufferR[FRAME_SIZE];

/*
 * Buffer intermedio para enventanar se�al del frame entrada
 */
float gInputWindowed[FRAME_SIZE];

/*
 * Buffer de salida sintetizada
 */
float gSynthVoiceFrame[FRAME_SIZE];

/*
 * Arreglo para crear se�al de pulsos
 */
float gVariableFreqPulsesData[FRAME_SIZE];

/*
 * Periodo en muestras de la se�al de pulsos generada
 */
unsigned int gVariableFreqPulsesPeriod = 80;

/*
 * Buffer para ruido generado en tiempo real
 */
float gRandNoise[FRAME_SIZE];

/*
 * �ndice buffer de ruido blanco para llenarlo
 */
unsigned int gIdxPrandNoise;

/*
 * Fuente de es�imulo para filtro AR
 */
float *gpArInputBuffer = NULL;

/*------------------------ ARREGLOS PARA LPC -----------------------*/
/*
 * Arreglo con valores de autocorrelaci�n
 */
float gCorrelationsArray[LPC_ORDER + 1];

/*
 * Par�metros del lpc identificados con levinson()
 */
float gLpcARcoeffs[LPC_ORDER+1];

/*--------------- VARIABLES PARA CORREGIR AMPLITUD  -----------------*/
/*
 * Valor RMS del frame de entrada
 */
float gInputFrameRms;

/*
 * Valor RMS del frame sintetizado con arFilter
 */
float gSynthVoiceFrameRms;

/*
 * Factor de correcci�n de amplitud de se�al sintetizada
 */
float gSynthAmpFactor;

/*
 * Variable auxiliar para calcular RMS del canal de entada derecho
 */
float gAuxInputVoiceRms = 0.0;

/*
 * Valor RMS del canal de entada derecho
 */
float gInputVoiceRms = 0.0;

/*--------------- VARIABLES PARA CLASIFICACI�N V/U  -----------------*/

/*
 * Clasificador VU
 */
vuSound_e gVUanalysis = VU_VOICED;

/*
 * Selector de tipo de ruido
 */
noiseType_e gNoiseTypeSelector = NOISE_RAND;

/*
 * Modo de s�ntesis
 */
synthMode_e gSynthMode = SYNTHMODE_AUTOMATIC;

/*
 * Umbral RMS
 */
float gVUrmsThreshold = 0.000;


/*--------------- VARIABLES PARA MEJORAS DE CALIDAD -----------------*/

float gMicNoiseFloorRms = 0.000;

/*---------------------------------------------------------------------------*/
/* DEFINICI�N DE VARIABLES GLOBALES PARA PRUEBAS Y DEBUGGING */
/*---------------------------------------------------------------------------*/

/*
 * LPC par�metros de prueba 'a'
 */
float gLpcA[LPC_ORDER+1] = {
   1.000000000000000,
  -0.582606819084131,
   0.192116093188965,
   0.288607088091191,
   0.493535676031534,
   0.009828286172478,
  -0.190959469422861,
  -0.015447520819975,
   0.682408683890808,
  -0.383201985116514,
  -0.058132732768475,
  -0.015920507797531,
  -0.014047716756587,
  -0.054775473919274,
  -0.027721114114396,
  -0.029782070482511
};

/******************************************************************************
**      PRIVATE FUNCTION DECLARATIONS (PROTOTYPES)
******************************************************************************/
interrupt void interrupt4(void);
void processFrame(void);

/******************************************************************************
**      FUNCTION DEFINITIONS
******************************************************************************/

/***************************************************************************//**
*   \brief  MAIN
*
*   \param  Void.
*
*   \return Void.
*******************************************************************************/
int main(void)
 {
    /*----------------------------------------------------------------------*/
    // Inicializaci�n del Codec de audio
    L138_initialise_edma(CODEC_FS, CODEC_ADC_GAIN, DAC_ATTEN_0DB, CODEC_INPUT_CFG);
    /*----------------------------------------------------------------------*/
    /* Inicializaci�n de Pulsadores User 1 y User 2 */
    DLU_initPushButtons();
    /* Inicializaci� de LEDs */
    DLU_initLeds();
    DLU_writeLedD4(LED_OFF);
    DLU_writeLedD5(LED_OFF);
    DLU_writeLedD6(LED_OFF);
    DLU_writeLedD7(LED_OFF);
    DLU_initTicToc();

    /*----------------------------------------------------------------------*/
    /* Inicializaci�n a cero del vector que usa el filtro AR */
    memset(gSynthVoiceFrame, 0, sizeof(gSynthVoiceFrame));

    /*----------------------------------------------------------------------*/
    /* Inicializaci�n de algoritmo de Levinso-Durbin */
    levinson_init(LPC_ORDER);

    /*----------------------------------------------------------------------*/
    /* Background: procesamiento tras levantar bandera */
    while(1)
    {
        /* Retenci�n hasta que 'processingBufferIsFull' es alto */
        while (!gProcessingBufferIsFull);

        /* Una vez llenado el buffer, se procesa en 'processFrame()' */
        DLU_writeLedD4(LED_ON); // Ciclo de trabajo del LED4 indica uso approx de CPU
        DLU_tic();
        processFrame();
        DLU_toc();  // Tiempo de c�mputo total del frame
        DLU_writeLedD4(LED_OFF);
    }
}

/***************************************************************************//**
*   \brief  Funci�n de procesamiento de un frame (ventana de captura).
*           Debe ser llamada desde el background en el loop infinito del main.
*
*   \param  Void.
*
*   \return Void.
*******************************************************************************/
void processFrame(void)
{
    int idxBuffer;
    gFramesCounter+=1;
    gAuxInputVoiceRms = 0.0;
    /*-------------------------------------------------------------------*/
    /* PARA VISUALIZAR EN GR�FICO: presionando el PB2 actualizar� los GraphBuff */
    if ( DLU_readPushButton2 () ){
        DLU_enableSynchronicSingleCaptureOnAllGraphBuff();
    }

    /*-----------------------------------------------------------------------*/
    /* Copia a arreglo de entrada al proceso de voz */
    for (idxBuffer = 0; idxBuffer < (FRAME_SIZE) ; idxBuffer++)
    {
        gFloatInputBufferL[idxBuffer] = AIC_2_FLT( codecInputArrayAsInt16L(idxBuffer) );
        gFloatInputBufferR[idxBuffer] = AIC_2_FLT( codecInputArrayAsInt16R(idxBuffer) );
        DLU_appendGraphBuff1(gFloatInputBufferL[idxBuffer]);

    }

    /************************************************************************/
    // PROCESAMIENTO PARA IMPLEMENTAR AN�LISIS DE VOZ
    /*----------------------------------------------------------------------*/
    /* 1) Valor RMS de frame de voz */
    // gInputVoiceRms  =

    /*----------------------------------------------------------------------*/
    /* 2) Enventanamiento */


    /*----------------------------------------------------------------------*/
    /* 3) Autocorrelaci�n */


    /*----------------------------------------------------------------------*/
    /* 4) Determinaci�n V/U:
     * Debe actualizar 'gVUanalysis' con  'VU_VOICED' o 'VU_UNVOICED'
     * Para visualizaci�n tambi�n:
     * VOICED enciende el led 7 y
     * UNVOICED apaga el led 7 */

    //  Cuando es Voiced:
    //  gVUanalysis = VU_VOICED;
    //  DLU_writeLedD7(LED_ON);

    //  Cuando es Unvoiced:
    //  gVUanalysis = VU_UNVOICED;
    //  DLU_writeLedD7(LED_OFF);

    /************************************************************************/
    // PROCESAMIENTO PARA IMPLEMENTAR S�NTESIS DE VOZ
    /*----------------------------------------------------------------------*/
    /* 5) Levinson */
    if ( DLU_readToggleStatePB1() )
    {
        // No actualiza los par�metros lpc. Se retienen �ltimos y se fija RMS
        gInputFrameRms = 0.15;
        // Se sobrescribe forzando VOICED
        gVUanalysis = VU_VOICED;
    }
    else
    {
        // Se calculan los coeficientes del filtro IIR.
        // Llame 'levinson_computeCoeffs()'
    }

    /*----------------------------------------------------------------------*/
    /* 6) Generar se�ales de excitaci�n del filtro */

    /* Se�al de pulsos ajustables en frecuencia */
    excitation_generatePulses(gVariableFreqPulsesData, gVariableFreqPulsesPeriod, FRAME_SIZE);

    /* Ruido blanco calculado en tiempo real en arreglo 'gRandNoise' */
    // gRandNoise

    /*----------------------------------------------------------------------*/
    /* 7) Selecci�n de se�al de excitaci�n */
    switch(gSynthMode)
    {
    case SYNTHMODE_EXTERNALSOURCE:
        gpArInputBuffer = gFloatInputBufferR;               // AR <-- gFloatInputBufferR
        DLU_writeLedD5(LED_OFF);
        break;
        //------------------------------------------------
    case SYNTHMODE_AUTOMATIC: // Selecci�n seg�n criterio
        if ( gVUanalysis == VU_VOICED){
            gpArInputBuffer = gVariableFreqPulsesData;      // AR <-- gVariableFreqPulsesData
            DLU_writeLedD5(LED_ON);
            break;
        } // No hay brake si es UNVOICED y s� debe pasar
          // al siguiente caso UNVOICED
        //------------------------------------------------
    case SYNTHMODE_ALWAYS_UNVOICED:
        if ( gNoiseTypeSelector == NOISE_STATIC){
            gpArInputBuffer = excitationNoise160;           // AR <-- excitationNoise160
            DLU_writeLedD6(LED_OFF);
        }
        if ( gNoiseTypeSelector == NOISE_RAND){
            gpArInputBuffer = gRandNoise;                  // AR <-- gRandNoise
            DLU_writeLedD6(LED_ON);
        }
        break;
        //------------------------------------------------
    case SYNTHMODE_ALWAYS_VOICED: // Siempre U
        // Si brake por que es tambi�n la opci�n default
    default:
        gpArInputBuffer = gVariableFreqPulsesData;          // AR <-- gVariableFreqPulsesData
        DLU_writeLedD5(LED_ON);
        //------------------------------------------------
    }

    /*----------------------------------------------------------------------*/
    /* 8) Filtrado */
    arFilter(&gSynthVoiceFrameRms, gSynthVoiceFrame, gLpcARcoeffs, LPC_ORDER, gpArInputBuffer, FRAME_SIZE); // excitation_pulses_100 gVariableFreqPulsesData

    /*----------------------------------------------------------------------*/
    /* 9) Correcci�n de amplitud a se�al de entrada */
    // gSynthAmpFactor

    /************************************************************************/
    /*----------------------------------------------------------------------*/
    // Escritura en el buffer de salida para el DMA
    for (idxBuffer = 0; idxBuffer < (FRAME_SIZE) ; idxBuffer++)
    {
        float auxSample = gSynthAmpFactor*gSynthVoiceFrame[idxBuffer];
        /* Canal izquierdo */
        codecOutputArrayAsInt16L(idxBuffer) = FLT_2_AIC(auxSample);
        /* Canal derecho igual a izquierdo */
        codecOutputArrayAsInt16R(idxBuffer) = FLT_2_AIC(auxSample);
        /* En caso de actvar la captura en buffer de gr�fico */
        DLU_appendGraphBuff2(auxSample);
    }
    /*----------------------------------------------------------------------*/
    /* Se baja flag de procesamiento*/
    gProcessingBufferIsFull = 0;
    return;
}

/***************************************************************************//**
*   \brief  Llenado de buffers con DMA
*
*   \param  Void.
*
*   \return Void.
*******************************************************************************/
interrupt void interrupt4(void) // interrupt service routine
{
    switch(EDMA_3CC_IPR)
    {
        case 1:                     // TCC = 0
            /* currentProcessingBuffer = PING */
            pInputPairBuffer = pingIN;
            pOutputPairBuffer = pingOUT;
            EDMA_3CC_ICR = 0x0001;    // clear EDMA3 IPR bit TCC
            break;
        case 2:                     // TCC = 1
            /* currentProcessingBuffer = PONG */
            pInputPairBuffer = pongIN;
            pOutputPairBuffer = pongOUT;
            EDMA_3CC_ICR = 0x0002;    // clear EDMA3 IPR bit TCC
            break;
        default:                    // may have missed an interrupt
            EDMA_3CC_ICR = 0x0003;    // clear EDMA3 IPR bits 0 and 1
            break;
    }

    /* Se baja flag de interrupci�n */
    EVTCLR0 = 0x00000100;

    /* Se levanta flag de buffer lleno */
    gProcessingBufferIsFull = 1;

    return;
}

/******************************************************************************
**      END OF SOURCE FILE
******************************************************************************/

